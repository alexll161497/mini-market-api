/**
 * 
 */
package com.pe.ms.mercado.web.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.pe.ms.mercado.core.base.exception.BadRequestException;
import com.pe.ms.mercado.core.base.exception.EmptyResultException;
import com.pe.ms.mercado.core.base.exception.ErrorDetails;
import com.pe.ms.mercado.core.base.exception.ErrorDetailsFields;
import com.pe.ms.mercado.core.base.exception.ErrorFunctionalException;
import com.pe.ms.mercado.core.base.exception.ResourceNotFoundException;
import com.pe.ms.mercado.core.base.resource.request.RequestPostClase;
import com.pe.ms.mercado.core.base.resource.request.RequestPutClase;
import com.pe.ms.mercado.core.base.resource.response.ResponseGetClase;
import com.pe.ms.mercado.core.base.resource.response.ResponsePostClase;
import com.pe.ms.mercado.core.base.resource.response.ResponsePutClase;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


/**
 * @author emaguina
 *
 */
@RequestMapping("/classes")
public interface ClaseController {

	@ApiOperation(value = "Get Classes", notes = "Service that allows you to search product class in the database", response = ResponseGetClase.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation Success.", response = ResponseGetClase.class),
			@ApiResponse(code = 204, message = "List Empty.", response = void.class) })
	@GetMapping(value = "/all")
	ResponseGetClase getClasses(
			@ApiParam(value = "Text to search for a Class by name or description", required = false) @RequestParam(value = "text", required = false) String text,
			@NotNull @ApiParam(value = "Page to show.", required = false) @RequestParam(value = "page", required = false) String page,
			@NotNull @ApiParam(value = "Number of records for page.", required = false) @RequestParam(value = "size", required = false) String size,
			@NotNull @ApiParam(value = "Field and type by which you want to order.", required = false) @RequestParam(value = "sortBy", required = false) String sortBy)
			throws EmptyResultException, BadRequestException;
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@ApiOperation(value = "Create Class", notes = "Service that allows create a class", response = ResponsePostClase.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Error functional.", response = ErrorDetails.class),
			@ApiResponse(code = 201, message = "Operation Success.", response = ResponsePostClase.class),
			@ApiResponse(code = 400, message = "The request could not be interpreted by an invalid syntax.", response = ErrorDetailsFields.class),
			@ApiResponse(code = 404, message = "The server could not find value in master table, for example country.", response = ErrorDetails.class) })
	@PostMapping(value = "")
	ResponsePostClase createClass(
			@ApiParam(value = "Class information that is necessary to add it to the system.", required = true) @Valid @RequestBody(required = false) RequestPostClase requestPostDto)
			throws ResourceNotFoundException, BadRequestException, ErrorFunctionalException;
	
	@ApiOperation(value = "Update Class", notes = "Service that allows update a class", response = ResponsePutClase.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Error functional.", response = ErrorDetails.class),
			@ApiResponse(code = 201, message = "Operation Success.", response = ResponsePutClase.class),
			@ApiResponse(code = 400, message = "The request could not be interpreted by an invalid syntax.", response = ErrorDetailsFields.class),
			@ApiResponse(code = 404, message = "The server could not find value in master table, for example country.", response = ErrorDetails.class) })
	@PutMapping(value = "/{class-id}")
	ResponsePutClase updateClase(
			@ApiParam(value = "Class Id", required = true) @PathVariable("class-id") Long customerId,
			@ApiParam(value = "Class information that is necessary to update it to the system.", required = true) @Valid @RequestBody(required = false) RequestPutClase requestPutDto)
			throws ResourceNotFoundException, BadRequestException, ErrorFunctionalException;
}
