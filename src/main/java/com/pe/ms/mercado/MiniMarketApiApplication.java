package com.pe.ms.mercado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.pe.ms.mercado")
@ConfigurationPropertiesScan("com.pe.ms.mercado.common.property")
public class MiniMarketApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniMarketApiApplication.class, args);
	}

}
