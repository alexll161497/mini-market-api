/**
 * ResponseGetClase.java 16-feb-2021
 *
 * Copyright 2021
 */
package com.pe.ms.mercado.core.base.resource.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Gets the data.
 *
 * @return the data
 */
@Getter

/**
 * Sets the data.
 *
 * @param data the new data
 */
@Setter
public class ResponsePostClase extends MessageResource {
	
	/** The data. */
	@JsonProperty("data")
	private ResponseDataPostClase data;
}
