/**
 * 
 */
package com.pe.ms.mercado.core.service;

import java.util.List;

import com.pe.ms.mercado.core.base.dto.ClaseDto;
import com.pe.ms.mercado.core.base.exception.BadRequestException;
import com.pe.ms.mercado.core.base.exception.EmptyResultException;
import com.pe.ms.mercado.core.base.exception.ErrorFunctionalException;
import com.pe.ms.mercado.core.base.exception.ResourceNotFoundException;

/**
 * The Interface ClaseService.
 *
 * @author emaguina
 */
public interface ClaseService {

	/**
	 * Gets the classes.
	 *
	 * @param text   the text
	 * @param page   the page
	 * @param size   the size
	 * @param sortBy the sort by
	 * @return the classes
	 * @throws BadRequestException  the bad request exception
	 * @throws EmptyResultException the empty result exception
	 */
	public List<ClaseDto> getClasses(String text, int page, int size, String sortBy)
			throws BadRequestException, EmptyResultException;

	/**
	 * Gets the count classes.
	 *
	 * @return the count classes
	 */
	public int getCountClasses();

	/**
	 * Creates the class.
	 *
	 * @param claseDto the clase dto
	 * @return the clase dto
	 * @throws ErrorFunctionalException the error functional exception
	 */
	public ClaseDto createClass(ClaseDto claseDto) throws ErrorFunctionalException;

	/**
	 * Update class.
	 *
	 * @param classId  the class id
	 * @param claseDto the clase dto
	 * @return the clase dto
	 * @throws ResourceNotFoundException the resource not found exception
	 * @throws BadRequestException the bad request exception
	 * @throws ErrorFunctionalException the error functional exception
	 */
	public ClaseDto updateClass(Long classId, ClaseDto claseDto)
			throws ResourceNotFoundException, BadRequestException, ErrorFunctionalException;
}
