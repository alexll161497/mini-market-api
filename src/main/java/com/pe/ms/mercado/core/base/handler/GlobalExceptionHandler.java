package com.pe.ms.mercado.core.base.handler;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.pe.ms.mercado.core.base.exception.BadRequestException;
import com.pe.ms.mercado.core.base.exception.CustomBadRequestException;
import com.pe.ms.mercado.core.base.exception.EmptyResultException;
import com.pe.ms.mercado.core.base.exception.ErrorDetails;
import com.pe.ms.mercado.core.base.exception.ErrorDetailsFields;
import com.pe.ms.mercado.core.base.exception.ErrorDetailsFunctional;
import com.pe.ms.mercado.core.base.exception.ErrorFunctionalException;
import com.pe.ms.mercado.core.base.exception.MyFileNotFoundException;
import com.pe.ms.mercado.core.base.exception.ResourceNotFoundException;
import com.pe.ms.mercado.util.Constante;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<org.springframework.validation.FieldError> fieldErrors = result.getFieldErrors();
		ErrorDetailsFields errorDetails = new ErrorDetailsFields(400, Constante.INVALID_FIELDS);
		for (org.springframework.validation.FieldError fieldError : fieldErrors) {
			errorDetails.agregarCampoError(fieldError.getDefaultMessage(), fieldError.getField());
		}
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(CustomBadRequestException.class)
	public ResponseEntity<?> CustomBadRequestException(CustomBadRequestException ex) {
		ErrorDetailsFields errorDetails = new ErrorDetailsFields(400, Constante.INVALID_FIELDS);
		errorDetails.setCampos(ex.getCamposErroneos());
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(MyFileNotFoundException.class)
	public ResponseEntity<?> myFileNotFoundException(MyFileNotFoundException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(EmptyResultException.class)
	public ResponseEntity<?> emptyResultException(EmptyResultException ex, WebRequest request) {
		return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> globleExcpetionHandler(Exception ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ErrorFunctionalException.class)
	public ResponseEntity<?> ErrorFunctionalException(Exception ex, WebRequest request) {
		ErrorDetailsFunctional errorDetails = new ErrorDetailsFunctional(ex.getMessage(), false);
		return new ResponseEntity<>(errorDetails, HttpStatus.OK);
	}

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<?> BadRequestException(Exception ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<?> CustomParseException(Exception ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getCause().getCause().getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<?> NumberFormatException(Exception ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }
}