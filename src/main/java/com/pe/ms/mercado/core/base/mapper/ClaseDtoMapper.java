package com.pe.ms.mercado.core.base.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.pe.ms.mercado.core.base.dto.ClaseDto;
import com.pe.ms.mercado.core.base.model.entity.Clase;


/**
 * The Interface ClaseDtoMapper.
 */
@Mapper
public interface ClaseDtoMapper {

	/**
	 * As clase dto.
	 *
	 * @param src the src
	 * @return the clase dto
	 */
	public ClaseDto asClaseDto(Clase src);

	/**
	 * As clase dtos.
	 *
	 * @param src the src
	 * @return the list
	 */
	public List<ClaseDto> asClasesDto(List<Clase> src);

	/**
	 * As clase.
	 *
	 * @param src the src
	 * @return the clase
	 */
	public Clase asClase(ClaseDto src);
}