package com.pe.ms.mercado.core.base.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class EmptyResultException.
 */
public class EmptyResultException extends Exception{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new empty result exception.
	 */
	public EmptyResultException(){
    	super();
    }
}
