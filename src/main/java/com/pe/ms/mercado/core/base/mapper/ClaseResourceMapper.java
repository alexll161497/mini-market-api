package com.pe.ms.mercado.core.base.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.pe.ms.mercado.core.base.dto.ClaseDto;
import com.pe.ms.mercado.core.base.resource.ClaseResource;
import com.pe.ms.mercado.core.base.resource.request.RequestPostClase;
import com.pe.ms.mercado.core.base.resource.request.RequestPutClase;

// TODO: Auto-generated Javadoc
/**
 * The Interface ClaseResourceMapper.
 */
@Mapper
public interface ClaseResourceMapper {

	/**
	 * As clase resource.
	 *
	 * @param src the src
	 * @return the clase resource
	 */
	public ClaseResource asClaseResource(ClaseDto src);

	/**
	 * As clase resources.
	 *
	 * @param src the src
	 * @return the list
	 */
	public List<ClaseResource> asClaseResources(List<ClaseDto> src);
	
	/**
	 * To dto.
	 *
	 * @param src the src
	 * @return the clase dto
	 */
	public ClaseDto asClaseDto(RequestPostClase src);
	
	/**
	 * As clase dto.
	 *
	 * @param src the src
	 * @return the clase dto
	 */
	public ClaseDto asClaseDto(RequestPutClase src);
}