package com.pe.ms.mercado.util;

public class Constante {

	// Handlers
	public static final String INVALID_FIELDS = "Campos Inválidos";

	// GENERICS
	public static final String RESOURCE_PACKAGE = "com.pe.ws.store.web.%s.resource";
	public static final String ESTABLISHMENT_BASE_RESOURCE = "EstablishmentResource";
	public static final String PRODUCT_TEMPLATE_BASE_RESOURCE = "ProductTemplateResource";
	public static final String SCHEDULE_TEMPLATE_BASE_RESOURCE = "ScheduleTemplateMasterResource";
	public static final String STOREPRODUCTS_BASE_RESOURCE = "StoreProductsResource";
	public static final String ORDER_BASE_RESOURCE = "OrderResource";
	public static final String CLASS_BASE_RESOURCE = "ClassResource";
	public static final String CATEGORY_BASE_RESOURCE = "CategoryResource";
	public static final String SUBCATEGORY_BASE_RESOURCE = "SubCategoryResource";
	public static final String SUBSUBCATEGORY_BASE_RESOURCE = "SubSubCategoryResource";
	public static final String STATE_ACTIVE = "A";
	public static final String STATE_INACTIVE = "I";
	public static final String COMA = ",";
	public static final String COMA_SPACE = " , ";
	public static final String SPACE = " ";
	public static final String DOT = ".";
	public static final String STOCK_PRODUCT_EXISTS = "S";
	public static final String JASYPT_PASSWORD = "some_salt";
	public static final String LINE_BREAK_HTML = "<br>";
	public static final String Y_SPACE = " y ";

	// TABLES
	public static final String TABLE_CUSTOMER = "customer";
	public static final String TABLE_ESTABLISHMENT = "establishment";
	public static final String TABLE_CLASS = "class";
	public static final String TABLE_CATEGORY = "category";
	public static final String TABLE_SUB_CATEGORY = "sub_category";
	public static final String TABLE_SUB_SUB_CATEGORY = "sub_sub_category";
	public static final String TABLE_PRODUCT_TEMPLATE = "product_template";
	

	// SHOPPING CART STATUS
	public static final String SHOPPING_CART_STATE_PROCESSED = "P";

	// ORDER
	public static final String IS_CUSTOMER = "C";
	public static final String IS_ESTABLISHMENT = "E";
	public static final String ORDER_SELL = "VENTA";
	public static final String ORDER_PURCHASE = "COMPRA";
	
	// ORDER STATUS
	public static final String ORDER_STATE_INFORMED = "I";
	public static final String ORDER_STATE_CANCELED = "N";
	public static final String ORDER_STATE_ATTENDED = "A";
	public static final String ORDER_STATE_REJECTED = "R";
	public static final String ORDER_STATE_SENT = "E";
	public static final String ORDER_STATE_DELIVERED = "T";

	// ORDER DETAIL STATUS
	public static final String ORDER_DETAIL_STATE_ATTENDED = "A";
	public static final String ORDER_DETAIL_STATE_NOT_ATTENDED = "N";

	// Filters
	public static final String NUMERO_PAGINA = "0";
	public static final String TAMANO_PAGINA = "1000000";
	public static final String ORDEN_ASC = "ASC";
	public static final String ORDEN_DESC = "DESC";
	public static final String ORDEN_ASC_MAS = "+";
	public static final String ORDEN_ASC_MAS_PARAM = "";
	public static final String ORDEN_DESC_MENOS = "-";
	public static final String EMPTY = "";

	// AMAZON S3
	public static final String BUCKET_NAME = "store-products-pe";
	public static final String FOLDER_CUSTOMER = "uploads/Customers/";
	public static final String FOLDER_ORDER = "uploads/Orders/";
	public static final String FOLDER_ESTABLISHMENTS = "uploads/Establishments/";
	public static final String FOLDER_CLASS = "uploads/Classes/";
	public static final String FOLDER_CATEGORY = "uploads/Categories/";
	public static final String FOLDER_SUB_CATEGORY = "uploads/SubCategories/";
	public static final String FOLDER_SUB_SUB_CATEGORY = "uploads/SubSubCategories/";
	public static final String FOLDER_PRODUCT = "uploads/Products/";
	public static final String UNDERLINE = "_";
	public static final String GUION = "-";
	public static final String FILE_EXTENSION_JPG = "jpg";
	public static final String FILE_EXTENSION_EXCEL = "xlsx";
	public static final String S3_CONTENT_TYPE_IMAGE = "image/jpeg";
	public static final String S3_CONTENT_TYPE_EXCEL = "application/vnd.ms-excel";
	public static final Long S3_CONTENT_LENGTH_IMAGE = 30190L;

	// MESSAGES ERROR
	public static final String LOGIN_SUCCESS = "generic.login-success.message.success";
	public static final String LOGIN_ERROR = "generic.login-error.message.error";
	public static final String MESSAGE_EMAIL_EXISTS = "generic.email-exists.message.error";
	public static final String MESSAGE_DNI_EXISTS = "generic.dni-exists.message.error";
	public static final String MESSAGE_NAME_EXISTS = "generic.name-exists.message.error";
	public static final String MESSAGE_COUNTRY_NOT_FOUND = "generic.country-not-found.message.error";
	public static final String MESSAGE_BODY_IS_REQUIRED = "generic.body-is-required.message.error";
	public static final String MESSAGE_PARAMETERS_PAGING_INVALID_TEXT = "Paging parameters are incorrect.";
	public static final String MESSAGE_PARAMETERS_PAGING_INVALID = "generic.parameters-paging-invalid.message.error";
	public static final String MESSAGE_ORDER_DETAIL_LIST_IS_REQUIRED = "generic.order-detail-list-is-required.message.error";
	public static final String MESSAGE_ORDER_DETAIL_LIST_NOT_ALLOWED = "generic.order-detail-list-not-allowed.message.error";
	public static final String MESSAGE_ORDER_DETAIL_PRODUCT_NOT_FOUND = "generic.order-detail-product-not-found.message.error";
	public static final String MESSAGE_ORDER_DETAIL_PRODUCT_IS_ALREADY = "generic.order-detail-product-is-already.message.error";
	public static final String MESSAGE_ORDER_DETAIL_CHANGE_PRODUCT_IS_EQUALS = "generic.order-detail-change-product-is-equals.message.error";
	public static final String MESSAGE_ORDER_DIFFERENT_INFORMED_NOT_ALLOWED = "generic.order-different-informed-not-allowed.message.error";
	public static final String MESSAGE_ORDER_IS_ALREADY_CANCELLED = "generic.order-is-already-cancelled.message.error";
	public static final String MESSAGE_ORDER_TRANSITION_STATUS_ERROR = "generic.order-transition-status.message.error";
	public static final String MESSAGE_LOGIN_USER_TYPE_NOT_ALLOWED = "generic.login-user-type-not-allowed.message.error";
	public static final String MESSAGE_ESTABLISHMENT_SHIPPING_SCHEDULE_NOT_FOUND = "establishment.shipping-schedule-not-found.message.error";
	public static final String MESSAGE_ESTABLISHMENT_PRODUCTS_NOT_FOUND = "establishment.products-not-found.message.error";
	public static final String MESSAGE_ESTABLISHMENT_PRODUCTS_EXISTS = "establishment.products-exists.message.error";
	public static final String MESSAGE_ESTABLISHMENT_OPERATION_SCHEDULE_NOT_FOUND = "establishment.operation-schedule-not-found.message.error";
	public static final String MESSAGE_ESTABLISHMENT_TYPE_NOT_FOUND = "establishment.type-not-found.message.error";
	public static final String MESSAGE_ESTABLISHMENT_NOT_FOUND = "establishment.id-not-found.message.error";
	public static final String MESSAGE_CUSTOMER_NOT_FOUND = "customer.id-not-found.message.error";
	public static final String MESSAGE_ORDER_STATE_NOT_ALLOWED = "establishment.order-state-not-allowed.message.error";
	public static final String MESSAGE_TABLE_NAME_NOT_ALLOWED = "generic.table-name-not-allowed.message.error";
	public static final String MESSAGE_ORDER_DETAIL_STATE_NOT_ALLOWED = "establishment.order-detail-state-not-allowed.message.error";
	public static final String MESSAGE_ESTABLISHMENT_ORDER_NOT_FOUND = "establishment.order-id-not-found.message.error";
	public static final String MESSAGE_PRODUCT_DETAIL_ERROR = "product.detail.message.error";
	public static final String MESSAGE_PRODUCT_STOCK_ZERO = "product.detail-stock-zero.message.error";
	public static final String MESSAGE_PRODUCT_FROM_ESTABLISHMENT_NOT_FOUND = "product.from-establishment-not-found.message.error";
	public static final String MESSAGE_CLASS_NOT_FOUND = "class.id-not-found.message.error";
	public static final String MESSAGE_CATEGORY_NOT_FOUND = "category.id-not-found.message.error";
	public static final String MESSAGE_EMAIL_NOT_EXISTS = "generic.email-not-exists.message.error";
	public static final String MESSAGE_SUB_CATEGORY_NOT_FOUND = "subCategory.id-not-found.message.error";
	public static final String MESSAGE_SUB_SUB_CATEGORY_NOT_FOUND = "subSubCategory.id-not-found.message.error";
	public static final String MESSAGE_PRODUCT_NOT_FOUND = "product.id-not-found.message.error";
	public static final String MESSAGE_EMAIL_NOT_SEND = "generic.email-not-send.message.error";

	// MESSAGES SUCCESS
	public static final String MESSAGE_IMAGE_UPLOAD_SUCCESS = "generic.image-upload.message.success";
	public static final String MESSAGE_CUSTOMER_CREATED_SUCCESS = "customer.registered.message.success";
	public static final String MESSAGE_CUSTOMER_UPD_UDPATED_SUCCESS = "customer.updated.message.success";
	public static final String MESSAGE_CUSTOMER_GENERATE_ORDER_SUCCESS = "customer.generate-order.message.success";
	public static final String MESSAGE_ESTABLISHMENT_CREATED_SUCCESS = "establishment.registered.message.success";
	public static final String MESSAGE_ESTABLISHMENT_UPD_UDPATED_SUCCESS = "establishment.updated.message.success";
	public static final String MESSAGE_ESTABLISHMENT_UPDATED_SUCCESS = "establishment.order-updated.message.success";
	public static final String MESSAGE_ESTABLISHMENT_CHANGE_PRODUCT_SUCCESS = "establishment.change-product.message.success";
	public static final String MESSAGE_ESTABLISHMENT_UPDATE_STORE_PRODUCT_SUCCESS = "establishment.update-store-product.message.success";
	public static final String MESSAGE_ESTABLISHMENT_ADD_PRODUCTS_SUCCESS = "establishment.add-products.message.success";
	public static final String MESSAGE_ESTABLISHMENT_GET_LIST = "establishment.get-establishment.messages.success";
	public static final String MESSAGE_ESTABLISHMENT_TREE_PRODUCTS_GET_LIST = "establishment.get-establishment-tree-products.messages.success";
	public static final String MESSAGE_ESTABLISHMENT_PRODUCTS_GET_LIST = "establishment.get-establishment-products.messages.success";
	public static final String MESSAGE_ESTABLISHMENT_CLASSES_GET_LIST = "establishment.get-establishment-classes.messages.success";
	public static final String MESSAGE_ESTABLISHMENT_CATEGORIES_GET_LIST = "establishment.get-establishment-categories.messages.success";
	public static final String MESSAGE_ESTABLISHMENT_SUBCATEGORIES_GET_LIST = "establishment.get-establishment-subcategories.messages.success";
	public static final String MESSAGE_ESTABLISHMENT_SUBSUBCATEGORIES_GET_LIST = "establishment.get-establishment-subsubcategories.messages.success";
	public static final String MESSAGE_PRODUCT_DETAIL_SUCCESS = "product.detail.message.success";
	public static final String MESSAGE_ESTABLISHMENT_ORDERS_GET_LIST = "establishment.get-orders.messages.success";
	public static final String MESSAGE_CUSTOMER_ORDERS_GET_LIST = "customer.get-orders.messages.success";
	public static final String MESSAGE_EXPORT_EXCEL_SUCCESS = "customer.order-export-excel.message.success";
	public static final String MESSAGE_PRODUCTS_GET_LIST = "product.get-products.messages.success";
	public static final String MESSAGE_SCHEDULE_TEMPLATE_GET_LIST = "base.get-schedules.messages.success";
	public static final String MESSAGE_CLASSES_GET_LIST = "class.get-classes.messages.success";
	public static final String MESSAGE_CLASS_CREATED_SUCCESS = "class.registered.message.success";
	public static final String MESSAGE_CLASS_UPDATED_SUCCESS = "class.updated.message.success";
	public static final String MESSAGE_CATEGORIES_GET_LIST = "category.get-categories.messages.success";
	public static final String MESSAGE_SUBCATEGORIES_GET_LIST = "subcategory.get-subcategories.messages.success";
	public static final String MESSAGE_SUBSUBCATEGORIES_GET_LIST = "subsubcategory.get-subsubcategories.messages.success";
	
	//MAIL
	public static final String MESSAGE_MAIL_SEND_SUCCESS = "mail.send-mail.message.success";
	public static final String MESSAGE_MAIL_TO = "mail.mail-to.message";
	public static final String MESSAGE_MAIL_NAME_SYSTEM = "mail.name-system.message";
	
	public static final String MESSAGE_MAILSUBJECT_FORGET_PASS = "mail.mail-subject-forget.message";
	public static final String MESSAGE_MAILSUBJECT_DEAR_FORGET = "mail.dear-forget.message";
	public static final String MESSAGE_MAIL_CUSTOMER_PASS = "mail.customer-password.message";
	public static final String MESSAGE_MAIL_ESTABLISHMENT_PASS = "mail.establishment-password.message";
	
	public static final String MESSAGE_MAILSUBJECT_CHANGE_PASS = "mail.mail-subject-change.message";
	public static final String MESSAGE_MAILSUBJECT_DEAR_CHANGE = "mail.mail-dear-change.message";
	public static final String MESSAGE_MAIL_CHANGE_PASS = "mail.mail-password-change.message";
	public static final String MESSAGE_MAIL_CHANGE_SEND_SUCCESS = "mail.send-mail-change.message.success";
	
	public static final String MESSAGE_MAIL_TEMPLATE_ATTE = "mail.template-attentively.message";
	public static final String MESSAGE_MAIL_TEMPLATE_THANKS = "mail.template-thanks.message";
	public static final String MESSAGE_MAIL_LOCATION_SYSTEM = "mail.localion-system.message";
	public static final String MESSAGE_MAIL_COPY_RIGHT = "mail.copyright-system.message";
	
}
